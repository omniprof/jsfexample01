package com.kenfogel.jsfexample01.business;

import com.kenfogel.jsfexample01.bean.UserBean;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the backing action bean for the form. Here an action is called on the
 * xhtml page by referencing the name, userIOBean, with the method name,
 * addRecord, to become #{userIOBean.addRecord}. It is in Session scope so that
 * every visitor shares the same bean as we want to write to the same file. The
 * method addRecord is synchronized so that the writes complete for each visitor
 * before the next visitor add to the file.
 *
 * @author Ken Fogel
 */
@Named("userIOBean")
@SessionScoped
public class UserIO implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(UserIO.class);

    // The magic of CDI. The bean that was bound to the form that issues the 
    // action will be the bean that is injected.
    @Inject
    private UserBean userBean;

    /**
     * Creates a new instance of UserIOBean
     */
    public UserIO() {
        super();
    }

    /**
     * This method writes the data in the userBean to a file. In previous years
     * I taught that it was possible to write this file into the folder that
     * contained the web application. This is no longer the case as its a bad
     * practice. You must never write into the directory structure of a web app.
     * Instead you should write to an absolute location that can be configured
     * in the web.xml file.
     *
     * @return
     * @throws IOException
     */
    public synchronized String addRecord() throws IOException {
        //Retrive the context-param with the file path
        String filename = FacesContext.getCurrentInstance().getExternalContext().getInitParameter("EmailFile");
        LOG.debug("Filename = " + filename);
        Path emailList = Paths.get(filename);
        try (BufferedWriter writer = Files.newBufferedWriter(emailList, Charset.forName("UTF-8"), StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            writer.write(userBean.getEmail() + "|"
                    + userBean.getFirstName() + "|"
                    + userBean.getLastName() + "\n");
        }
        return "success";
    }
}
