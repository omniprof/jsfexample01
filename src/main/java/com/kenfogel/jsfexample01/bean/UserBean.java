package com.kenfogel.jsfexample01.bean;

import java.io.Serializable;
import java.util.Objects;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * This is the backing data bean for the form. These fields are bound to the
 * xhtml page by referencing the name, userBean, with the field name, such as
 * firstName, to become #{userBean.firstName}. It is in Request scope so that
 * every visitor gets their own bean.
 *
 * @author Ken Fogel
 */
@Named("userBean")
@RequestScoped
public class UserBean implements Serializable {

    private String firstName;
    private String lastName;
    private String email;

    public UserBean() {
        super();
    }

    public UserBean(String first, String last, String email) {
        super();
        firstName = first;
        lastName = last;
        email = email;
    }

    public void setFirstName(String f) {
        firstName = f;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String l) {
        lastName = l;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmail(String e) {
        email = e;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.firstName);
        hash = 17 * hash + Objects.hashCode(this.lastName);
        hash = 17 * hash + Objects.hashCode(this.email);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserBean other = (UserBean) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        return Objects.equals(this.email, other.email);
    }

    @Override
    public String toString() {
        return "User{" + "firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + '}';
    }
}
